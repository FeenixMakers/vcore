# Rat Rig V-core 3 Klipper Config
# Documentation: https://os.ratrig.com

# 0) Sanity check and PID Tuning: https://www.klipper3d.org/Config_checks.html
# 1) Pressure Advance: https://www.klipper3d.org/Pressure_Advance.html
# 2) Skew Correction: https://www.klipper3d.org/Skew_Correction.html
# 3) Resonance Compensation: https://www.klipper3d.org/Resonance_Compensation.html

# Read more about klipper here: https://www.klipper3d.org/Overview.html
[include mainsail.conf]

[mcu]
baud: 250000
serial: /dev/btt-octopus-11

[temperature_sensor Octopus]
sensor_type: temperature_mcu
min_temp: 0
max_temp: 100

[temperature_sensor raspberry_pi]
sensor_type: temperature_host

[adxl345]
spi_bus: spi3
cs_pin: PA15

#############################################################################################################
### BASE SETUP
#############################################################################################################
[idle_timeout]
gcode:
  {% if printer.webhooks.state|lower == 'ready' %}
    {% if printer.pause_resume.is_paused|lower == 'false' %}
      M117 Idle timeout reached
      TURN_OFF_HEATERS
      M84
    {% endif %}
  {% endif %}
# 2 hour timeout
timeout: 7200

[skew_correction]

[input_shaper]

[virtual_sdcard]
path: ~/gcode_files

[display_status]

[pause_resume]

[force_move]
enable_force_move: True

[respond]

[heater_bed]
heater_pin: PA1
sensor_pin: PF3
sensor_type: Generic 3950
min_temp: 0
max_temp: 120
pwm_cycle_time: 0.02 # 50hz for european AC, to avoid flickering lights.

[fan]
pin: PA8
shutdown_speed: 1.0

[heater_fan toolhead_cooling_fan]
pin: PE5
fan_speed: 1

[controller_fan controller_fan]
pin: PD12

# These are only safeguards for first time users
# Modify printer.cfg to tune acceleration.
[printer]
kinematics: corexy
max_velocity: 150
max_accel: 500
max_accel_to_decel: 750
max_z_velocity: 15
max_z_accel: 20

#############################################################################################################
### STEPPER MOTORS, DRIVERS & SPEED LIMITS
### Pick the drivers and stepper motors you're using. See the RatOS documentation for custom combinations.
#############################################################################################################
[stepper_x]
enable_pin: !PF14
dir_pin: PF12
step_pin: PF13
endstop_pin: PG6
position_endstop: 0 # Adjust this to your setup
position_max: 500
rotation_distance: 40
microsteps: 64
homing_speed: 50
homing_retract_dist: 5.0

[stepper_y]
enable_pin: !PF15
step_pin: PG0
endstop_pin: PG9
dir_pin: PG1
position_min: 5
position_endstop: 495
position_max: 495
rotation_distance: 40
microsteps: 64
homing_speed: 50
homing_positive_dir: true
homing_retract_dist: 5.0

[stepper_z]
endstop_pin: probe:z_virtual_endstop
enable_pin: !PF1
step_pin: PC13
dir_pin: !PF0 # Add ! in front of pin name to reverse Z stepper direction
rotation_distance: 4
microsteps: 64
position_min: -5 # Needed for z-offset calibration and tilt_adjust.
homing_speed: 10
position_max: 500

[stepper_z1]
endstop_pin: probe: z_virtual_endstop
enable_pin: !PD4
step_pin: PE2
dir_pin: !PE3 # Add ! in front of pin name to reverse Z1 direction
rotation_distance: 4
microsteps: 64

[stepper_z2]
endstop_pin: probe: z_virtual_endstop
enable_pin: !PE0
step_pin: PE6
dir_pin: !PA14 # Add ! in front of pin name to reverse Z2 direction
rotation_distance: 4
microsteps: 64
rotation_distance: 4 # 4 for TR8*4 lead screws

[z_tilt]
speed: 200

[printer]
max_velocity: 200
max_accel: 1500
max_accel_to_decel: 750
max_z_velocity: 15
max_z_accel: 30
square_corner_velocity: 5

# Backwards compatibility

[tmc2209 stepper_x]
uart_pin: PC4
run_current: 1.1
stealthchop_threshold: 1
driver_TBL: 1
driver_TOFF: 3
driver_HEND: 0
driver_HSTRT: 0

[tmc2209 stepper_y]
uart_pin: PD11
run_current: 1.1
stealthchop_threshold: 1
driver_TBL: 1
driver_TOFF: 3
driver_HEND: 0
driver_HSTRT: 0

[tmc2209 stepper_z]
uart_pin: PE4
run_current: 1.0
stealthchop_threshold: 1
driver_TBL: 1
driver_TOFF: 3
driver_HEND: 0
driver_HSTRT: 0

[tmc2209 stepper_z1]
uart_pin: PE1
run_current: 1.1
stealthchop_threshold: 1
driver_TBL: 1
driver_TOFF: 3
driver_HEND: 0
driver_HSTRT: 0

[tmc2209 stepper_z2]
uart_pin: PD3
run_current: 1.0
stealthchop_threshold: 1
driver_TBL: 1
driver_TOFF: 3
driver_HEND: 0
driver_HSTRT: 0

[tmc2209 extruder]
uart_pin:  PC6
run_current: 0.35
stealthchop_threshold: 0


#############################################################################################################
### HOMING
### Pick your probe and endstops
#############################################################################################################
# BL Touch
[bltouch]
sensor_pin: ^PB7
control_pin: PB6
speed: 7
pin_move_time: 0.675
sample_retract_dist: 10
pin_move_time: 0.8
pin_up_reports_not_triggered: True
pin_up_touch_mode_reports_triggered: True
x_offset: -28
y_offset: -13



[safe_z_home]
home_xy_position: 150, 250 # Change coordinates to the center of your print bed
speed: 135
speed: 50
z_hop: 10                 # Move up 10mm
z_hop_speed: 5


[bed_mesh]
horizontal_move_z: 5
mesh_min: 15,15
mesh_max:465,460
probe_count: 28,13
fade_start: 10.0
fade_end: 100.0
mesh_pps: 2,2
algorithm: bicubic
bicubic_tension: .2

[z_tilt]
z_positions:
	0,0
	250,500
	500,0

points:
	60,60
	285,470
	460,60

horizontal_move_z: 12
retries: 10
retry_tolerance: 0.02


#############################################################################################################
### extruder and hotend
#############################################################################################################

[extruder]
step_pin: PF11
dir_pin: PG3
enable_pin: !PG5
microsteps: 64
rotation_distance: 4.63
full_steps_per_rotation: 200
filament_diameter: 1.750
max_extrude_only_velocity: 60
max_extrude_only_distance: 200
nozzle_diameter: 0.4
heater_pin: PA2
sensor_type: ATC Semitec 104GT-2
sensor_pin: PF4
min_extrude_temp: 170
min_temp: 0
max_temp: 400
pressure_advance: 0.05

[firmware_retraction]
retract_speed: 60
unretract_extra_length: 0
unretract_speed: 60
retract_length: 0.5



#############################################################################################################
### MACROS
#############################################################################################################
[include macros.cfg]
[include shell-macros.cfg]

# Print macros. Call these from your slicer (custom g-code).
# You can copy these to printer.cfg and modify them to your liking, or just use them as is.
# Read more here: https://rat-rig.github.io/V-CoreOS/#/slicers
[gcode_macro _START_PRINT_AFTER_HEATING_BED]
gcode:
  {% if printer["gcode_macro RatOS"].preheat_extruder|lower == 'true' %}
  M117 Pre-heating extruder...
  # Wait for extruder to reach 150 so an inductive probe (if present) is at a predictable temp.
  # Also allows the bed heat to spread a little, and softens any plastic that might be stuck to the nozzle.
  M104 S150
  TEMPERATURE_WAIT SENSOR=extruder MINIMUM=150
  {% endif %}
  M117 Adjusting for tilt...
  # Adjust bed tilt
  Z_TILT_ADJUST
  M117 Rehoming after tilt adjustment...
  # Home again as Z will have changed after tilt adjustment and bed heating.
  G28 Z


#############################################################################################################
### MACRO CONFIGURATION
### Configure the behavior of RatOS macros
#############################################################################################################
[gcode_macro RatOS]
# Wait for extruder to reach 150 so an inductive probe (if present) is at a predictable temp.
# Also allows the bed heat to spread a little, and softens any plastic that might be stuck to the nozzle.
# Set to False to disable
variable_preheat_extruder: True
# Print a prime line or blob at the end of the START_PRINT macro
# set to "primeline" or "primeblob", or False to disable nozzle_priming.
variable_nozzle_priming: "false"
# Park in the back when waiting for the extruder to heat up
# set to "front" to park in the front, or "center" to park in the center.
variable_start_print_park_in: "center"
# Height to park it when waiting for extruder to heat.
variable_start_print_park_z_height: 50
# Skew profile to load before starting the print
# uncomment this to use your calibrated skew correction profile.
#variable_skew_profile: "my_skew_profile"
# Park in the back after the print has ended or was cancelled.
# set to "front" to park in the front, or "center" to park in the center.
variable_end_print_park_in: "back"
# Park in the back when the print is paused.
# set to "front" to park in the front, or "center" to park in the center.
variable_pause_print_park_in: "back"
# Set the speed for travel moves in RatOS Macros in mm/s.
variable_macro_travel_speed: 100


#ORIGINAL
#home_xy_position: 150,150 # 300mm printer
#home_xy_position: 200,200 # 400mm printer
#home_xy_position: 250,250 # 500mm printer


#############################################################################################################
### INPUT SHAPER
### Enable/disable input shaper calibration
#############################################################################################################
# Uncomment this next line if you have an ADXL345 connected to your control board
#[include config/printers/v-core-3/input-shaper.cfg]

# ADXL345 resonance testing configuration
#[resonance_tester]
#probe_points:
#    150,150,20 # 300mm printer
#    200,200,20 # 400mm printer
#    250,250,20 # 500mm printer

#*# <---------------------- SAVE_CONFIG ---------------------->
#*# DO NOT EDIT THIS BLOCK OR BELOW. The contents are auto-generated.
#*#
#*# [bltouch]
#*# z_offset = 3.000
#*#
#*# [bed_mesh default]
#*# version = 1
#*# points =
#*# 	  0.754375, 0.690000, 0.584375, 0.520937, 0.427500, 0.382500, 0.296875, 0.255937, 0.217187, 0.182812, 0.166562, 0.170625, 0.171250, 0.174375, 0.160000, 0.178750, 0.225937, 0.248750, 0.248750, 0.270625, 0.313125, 0.373125, 0.390937, 0.415937, 0.460625, 0.522187, 0.607187, 0.655937
#*# 	  0.705937, 0.606250, 0.527812, 0.459062, 0.406250, 0.340000, 0.325000, 0.267500, 0.209062, 0.211875, 0.210937, 0.170937, 0.203437, 0.180000, 0.183437, 0.221250, 0.222500, 0.246875, 0.272500, 0.298437, 0.339062, 0.380000, 0.410312, 0.463750, 0.525937, 0.550937, 0.603750, 0.696875
#*# 	  0.710312, 0.605312, 0.542812, 0.472500, 0.412812, 0.349375, 0.332812, 0.300625, 0.274062, 0.262812, 0.244062, 0.239062, 0.201875, 0.230625, 0.223125, 0.253125, 0.256562, 0.321875, 0.314062, 0.337812, 0.383125, 0.414687, 0.459062, 0.497500, 0.550312, 0.592500, 0.646562, 0.704375
#*# 	  0.617500, 0.540625, 0.466875, 0.442500, 0.401562, 0.374375, 0.278125, 0.276875, 0.250937, 0.214375, 0.191562, 0.185937, 0.216875, 0.203437, 0.220312, 0.225312, 0.239375, 0.293125, 0.280625, 0.355937, 0.373750, 0.421562, 0.450937, 0.462187, 0.538125, 0.569062, 0.620937, 0.745000
#*# 	  0.451562, 0.397187, 0.365312, 0.303125, 0.262812, 0.213125, 0.162187, 0.164375, 0.170937, 0.158125, 0.150000, 0.182500, 0.170625, 0.155000, 0.180000, 0.230312, 0.197187, 0.280000, 0.272187, 0.302812, 0.368437, 0.421250, 0.443750, 0.472812, 0.521562, 0.608750, 0.651562, 0.743437
#*# 	  0.366875, 0.276562, 0.267187, 0.231875, 0.180937, 0.150312, 0.134062, 0.114062, 0.113125, 0.120937, 0.119375, 0.139062, 0.152187, 0.122500, 0.113750, 0.158125, 0.200937, 0.223437, 0.257812, 0.327500, 0.363437, 0.415625, 0.465937, 0.499375, 0.529062, 0.604375, 0.680937, 0.788125
#*# 	  0.264062, 0.240000, 0.179687, 0.126875, 0.118125, 0.094062, 0.046250, 0.067500, 0.046875, 0.048750, 0.103437, 0.072500, 0.092500, 0.124687, 0.150312, 0.160312, 0.238125, 0.235937, 0.279687, 0.345937, 0.366875, 0.405312, 0.460937, 0.516875, 0.590937, 0.602500, 0.690625, 0.764062
#*# 	  0.167500, 0.121875, 0.099375, 0.068750, 0.089062, 0.057812, -0.005625, -0.005625, 0.016562, -0.015938, 0.034687, 0.026250, 0.054062, 0.087812, 0.094687, 0.154375, 0.195625, 0.206875, 0.271250, 0.301875, 0.377812, 0.412500, 0.420937, 0.506250, 0.539687, 0.584687, 0.667812, 0.789375
#*# 	  0.161562, 0.105000, 0.111875, 0.080937, 0.053125, 0.053750, 0.033437, 0.090625, 0.010312, 0.075625, 0.040312, 0.110937, 0.128437, 0.122187, 0.144687, 0.196562, 0.207500, 0.256562, 0.274375, 0.368750, 0.374062, 0.417812, 0.508125, 0.527812, 0.580000, 0.637812, 0.715000, 0.765000
#*# 	  0.177812, 0.088437, 0.085000, 0.115625, 0.113125, 0.080000, 0.063750, 0.081250, 0.100000, 0.066875, 0.114062, 0.120312, 0.151250, 0.165000, 0.168750, 0.223750, 0.247500, 0.275937, 0.387500, 0.402187, 0.464375, 0.529062, 0.560312, 0.580625, 0.642812, 0.688125, 0.738750, 0.805937
#*# 	  0.221875, 0.178125, 0.161250, 0.175937, 0.179687, 0.118437, 0.122187, 0.157500, 0.133437, 0.153125, 0.174687, 0.202812, 0.175312, 0.229687, 0.272500, 0.312187, 0.358750, 0.378750, 0.478125, 0.505000, 0.514687, 0.557812, 0.599687, 0.683750, 0.715625, 0.767500, 0.824687, 0.896875
#*# 	  0.329687, 0.304062, 0.285625, 0.289062, 0.281250, 0.280625, 0.235312, 0.279375, 0.270625, 0.290625, 0.324062, 0.299062, 0.328125, 0.350000, 0.392500, 0.431562, 0.414375, 0.457812, 0.540625, 0.569062, 0.601250, 0.663750, 0.679062, 0.673437, 0.776562, 0.784375, 0.888125, 0.951250
#*# 	  0.463125, 0.423437, 0.413750, 0.410000, 0.426875, 0.381875, 0.400312, 0.408437, 0.412500, 0.415312, 0.434062, 0.473750, 0.502187, 0.562812, 0.526875, 0.604062, 0.614687, 0.636562, 0.731562, 0.732187, 0.786562, 0.805625, 0.842812, 0.906562, 0.967500, 0.988125, 1.064062, 1.178125
#*# tension = 0.2
#*# min_x = 15.0
#*# algo = bicubic
#*# y_count = 13
#*# mesh_y_pps = 2
#*# min_y = 15.0
#*# x_count = 28
#*# max_y = 459.96
#*# mesh_x_pps = 2
#*# max_x = 464.82
#*#
#*# [extruder]
#*# control = pid
#*# pid_kp = 19.661
#*# pid_ki = 0.819
#*# pid_kd = 117.969
#*#
#*# [heater_bed]
#*# control = pid
#*# pid_kp = 64.455
#*# pid_ki = 1.659
#*# pid_kd = 626.020
